<?php

namespace CodingPaws\GoesRPC\Error;

use Exception;

class RPCSocketException extends Exception
{
}
