<?php

namespace CodingPaws\GoesRPC;

use CodingPaws\GoesRPC\Base\RPCService as BaseRPCService;

abstract class RPCClient
{
  protected BaseRPCService $service;

  public function __construct(string $addr, int $port)
  {
    $this->service = $this->newService($addr, $port);
  }

  protected function newService(string $addr, int $port): BaseRPCService
  {
    return new RPCService($addr, $port);
  }
}
