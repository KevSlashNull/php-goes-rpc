<?php

namespace CodingPaws\GoesRPC;

use CodingPaws\GoesRPC\Base\RPCService as BaseRPCService;
use CodingPaws\GoesRPC\Error\RPCException;
use CodingPaws\Layer4\Base\DuplexConnection;
use CodingPaws\Layer4\ConnectionFactory;

class RPCService implements BaseRPCService
{
  private int $id = 0;
  private ?DuplexConnection $connection = null;
  private string $address;
  private string $port;

  public function __construct(string $address, int $port)
  {
    $this->address = $address;
    $this->port = $port;
  }

  public function connect(): void
  {
    if ($this->isConnected()) {
      return;
    }
    $this->connection = ConnectionFactory::tcp($this->address, $this->port);
  }

  public function isConnected(): bool
  {
    return $this->connection ? $this->connection->isConnected() : false;
  }

  public function send(string $method, $params)
  {
    $this->connect();

    $payload = [
      'method' => $method,
      'params' => [$params],
      'id' => $this->id++
    ];

    $json = json_encode($payload) . "\n";
    $this->connection->send($json);

    $read = "";

    while (!str_ends_with($read, "}\n")) {
      $result = $this->connection->read(1024 * 1024); // max. 1 MB

      if (is_null($result)) {
        break;
      }

      $read .= $result;
    }

    $data = json_decode($read, true);

    if ($data['error']) {
      throw new RPCException((string) $data['error']);
    }

    return $data['result'];
  }
}
