<?php

namespace CodingPaws\GoesRPC\Base;

interface RPCService
{
  public function send(string $method, $params);
  public function connect(): void;
  public function isConnected(): bool;
}
