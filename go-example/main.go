package main

import (
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type FruitShop struct{}

type Fruit string
type Fruits []Fruit

var fruits = Fruits{"apple", "pineapple", "orange"}

func (*FruitShop) CanSellFruit(fruitName string, response *bool) error {
	*response = fruits.Contains(fruitName)
	return nil
}

func main() {
	server := rpc.NewServer()
	server.Register(new(FruitShop))
	listener, e := net.Listen("tcp", "127.0.0.1:8080")

	if e != nil {
		log.Fatal("listen error:", e)
		return
	}

	for {
		if conn, err := listener.Accept(); err != nil {
			log.Fatal("accept error: " + err.Error())
		} else {
			go server.ServeCodec(jsonrpc.NewServerCodec(conn))
		}
	}
}

func (fruits Fruits) Contains(fruitName string) bool {
	for _, fruit := range fruits {
		if string(fruit) == fruitName {
			return true
		}
	}
	return false
}
