<?php

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
  throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

use CodingPaws\GoesRPC\RPCClient;

describe('example implementation (FruitShop)', function () {
  describe('a custom RPC implementation', function () {
    subject(fn () => new FruitShop("127.0.0.1", 8080));
    foreach (['apple', 'pineapple', 'orange'] as $fruit) {
      context("for a valid fruit $fruit", function () use ($fruit) {
        let('fruit', $fruit);

        it('returns true', function () {
          expect(subject()->CanSellFruit($this->fruit))->toBe(true);
        });
      });
    }

    context('for an invalid fruit', function () {
      it('returns false', function () {
        expect(subject()->CanSellFruit('onion'))->toBe(false);
      });
    });
  });

  describe('an unreachable client', function () {
    subject(fn () => new FruitShop("127.0.0.1", 8081));

    it('throws an error', function () {
      expect(function () {
        subject()->CanSellFruit("apple");
      })->toThrow();
    });
  });
});


class FruitShop extends RPCClient
{
  public function CanSellFruit(string $fruit): bool
  {
    return (bool) $this->service->send("FruitShop.CanSellFruit", $fruit);
  }
}
